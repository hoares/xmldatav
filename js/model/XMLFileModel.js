(function() {
    var XMLFileModel = Backbone.Model.extend({
	initialize: function(xmlString) {
	    console.log("init XMLFileModel");
	    this.xmlString = xmlString;
	},

	getData: function(xpathQuery) {
	    var root = $.parseXML(this.xmlString);
	    var nodes = $(root).xpath(xpathQuery);
	    return _.map(nodes, function(node) {
		return $(node).text().trim();
	    });
	}
    });

    window.XMLFileModel = XMLFileModel;
})();