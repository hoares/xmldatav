(function() {
    var ChartModel = Backbone.Model.extend({
	initialize: function(xmlFilesCollection) {
	    this.xmlFilesCollection = xmlFilesCollection;
	},
	
	setConfig: function(formData) {
	    var xAxisQuery = formData.xAxisQuery;
	    var yAxisQuery = formData.yAxisQuery;

	    var config = {
 		chart: {
		    type: 'column',
		    borderWidth: 1
		},
		title: {
		    text: formData.title
		},
		xAxis: {
		    categories: this.xmlFilesCollection.getData(xAxisQuery)
		},
		yAxis: {
		    title: {
			text: formData.yTitle
		    }
		},
		series: [
		    {
			name: formData.seriesName,
			data: _.map(this.xmlFilesCollection.getData(yAxisQuery), function(text) {
			    return parseInt(text);
			})
		    }
		]
	    };
	    this.set({config: config});
	}
    });

    window.ChartModel = ChartModel;
})();
