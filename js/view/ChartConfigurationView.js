(function() {
    var ChartConfigurationView = Backbone.View.extend({
	el: ".chart-config",
	
	events: {
	    "click .show-chart": "showChart"
	},
	
	showChart: function() {
	    var formData = {};
	    formData.title = $('input[name="title"]', this.$el).val();
	    formData.yTitle = $('input[name="ytitle"]', this.$el).val();
	    formData.xAxisQuery = $('input[name="xAxisQuery"]', this.$el).val();
	    formData.yAxisQuery = $('input[name="yAxisQuery"]', this.$el).val();
	    formData.seriesName = $('input[name="seriesName"]', this.$el).val();
	    console.dir(formData);
	    this.model.setConfig(formData);
	    return false;
	}
    });
    
    window.ChartConfigurationView = ChartConfigurationView;
})();
